import qbs 1.0
import qbs.FileInfo
import "omniidl.js" as Fn

Module {
name: "omniidl"
property string genDirName: ".omniidl"
property string genDirPath: product.buildDirectory + '/' + genDirName
Depends { name: "cpp"}
FileTagger {
    pattern: "*.idl"
    fileTags: ["idl"]
}
cpp.dynamicLibraries: ["omniORB4", "omnithread"]
cpp.includePaths: genDirPath
Rule {
	inputs: ['idl']
	Artifact {
        fileTags: ['cpp']
        fileName: Fn.genName(".cpp")
    }
	Artifact {
        fileTags: ['hpp']
        fileName:  Fn.genName(".h")
    }
	prepare: {
		var cmd =  new Command("omniidl",
				["-bcxx", "-Wbh=.h", "-Wbs=.cpp", input.fileName])
		cmd.description = "Generating omniorb stubs"
		cmd.workingDirectory = product.moduleProperty('omniidl', 'genDirPath')
		return cmd
	}
}
}
