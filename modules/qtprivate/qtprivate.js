function privateIncPaths(prefix, modules, version){
	var incPaths = []
	modules.sort()
	var prevItem = ""
	for (var i = 0; i < modules.length; i++){
		if (modules[i] == prevItem || modules[i] == "Core") delete modules[i]
		else prevItem = modules[i]	
	}
	incPaths.push(prefix + "/QtCore/" + version)
	incPaths.push(prefix + "/QtCore/" + version + "/QtCore")
	for (var i = 0; i < modules.length; i++){
		if (!modules[i]) continue
		var dir = "Qt" + modules[i]
			incPaths.push(prefix + "/" + dir + "/" + version)
	}
	return incPaths
}
