import qbs 1.0
import "qtprivate.js" as Fn

Module {
	Depends { name: "cpp" }
	Depends { name: "Qt";  submodules:["core"]}
	/*Use capitalized Qt modules names*/
	property var modules
	cpp.includePaths: 
		base.concat( Fn.privateIncPaths(
			Qt.core.incPath, modules, Qt.core.version))
}
